/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package converter;

import database.entities.SportFacilityE;
import database.service.SportsFacilitiesService;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

/**
 *
 * @author Gabo
 */
@ManagedBean
@SessionScoped
public class SportFacilityConverter implements Converter {

    @ManagedProperty(value="#{sportsFacilitiesService}")
    SportsFacilitiesService sportFacilitiyService;
    
    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        Long id = Long.valueOf(value);
        SportFacilityE sportFacility = sportFacilitiyService.getSportFacilityById(id);
        return sportFacility;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if(value != null && value instanceof SportFacilityE) { 
            return ((SportFacilityE)value).getId().toString();
        }  
        return null; 
    }   

    public SportsFacilitiesService getSportFacilitiyService() {
        return sportFacilitiyService;
    }

    public void setSportFacilitiyService(SportsFacilitiesService sportFacilitiyService) {
        this.sportFacilitiyService = sportFacilitiyService;
    }
    
}
