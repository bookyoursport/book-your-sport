/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package converter;

import database.entities.SportsE;
import database.service.SportsService;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

/**
 *
 * @author gcervena
 */
@ManagedBean
@SessionScoped
public class SportsConverter implements Converter {

    @ManagedProperty(value = "#{sportsService}")
    SportsService sportsService;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        SportsE sport;
        if (value.equals("Choose sport")) {
            sport = new SportsE("Choose sport");
        } else {
            Long id = Long.valueOf(value);
            sport = sportsService.getSportById(id);
        }
        return sport;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value != null && value instanceof SportsE) {
            if (((SportsE) value).getId() != null) {
                return ((SportsE) value).getId().toString();
            }
        }
        return null;
    }

    public SportsService getSportsService() {
        return sportsService;
    }

    public void setSportsService(SportsService sportsService) {
        this.sportsService = sportsService;
    }
}
