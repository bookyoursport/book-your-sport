/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package database.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author gcervena
 */
@Entity
public class UserE implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "EMAIL")
    private String email;
    @Column(name = "FIRST_NAME")
    private String firsname;
    @Column(name = "LAST_NAME")
    private String lastname;
    @Column(name = "PASSWORD")
    private String passwd;
    @Column(name = "PHONE_NUMBER")
    private String phone;
    @OneToMany
    private List<SportsE> sports;
    @OneToMany(mappedBy = "userE")
    private List<ReservationE> reservations;
    @Column(name = "IS_ADMIN")
    private boolean admin = false;

    public UserE (){
    }

    public UserE(String email, String firsname, String lastname, String passwd, String phone, List<SportsE> sports, List<ReservationE> reservations) {
        this.email = email;
        this.firsname = firsname;
        this.lastname = lastname;
        this.passwd = passwd;
        this.phone = phone;
        this.sports = sports;
        this.reservations = reservations;
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirsname() {
        return firsname;
    }

    public void setFirsname(String firsname) {
        this.firsname = firsname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @XmlTransient
    public List<ReservationE> getReservations() {
        return reservations;
    }

    public void setReservations(List<ReservationE> reservations) {
        this.reservations = reservations;
    }

    public List<SportsE> getSports() {
        return sports;
    }

    public void setSports(List<SportsE> sports) {
        this.sports = sports;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserE)) {
            return false;
        }
        UserE other = (UserE) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "database.entities.UserE[ id=" + id + " ]";
    }

    /**
     * @return the isAdmin
     */
    public boolean isAdmin() {
        return admin;
    }

    /**
     * @param isAdmin the isAdmin to set
     */
    public void setAdmin(boolean isAdmin) {
        this.admin = isAdmin;
    }
    
}
