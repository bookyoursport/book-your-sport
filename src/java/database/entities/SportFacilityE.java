/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package database.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.*;

/**
 *
 * @author gcervena
 */
@Entity
public class SportFacilityE implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "SPORTFAC_NAME")
    private String name;
    @OneToOne(cascade=CascadeType.ALL)
    private AddressE address;
    @OneToMany
    private List<SportsE> sports;

    public SportFacilityE() {
    }

    public SportFacilityE(String name, AddressE address, List<SportsE> sports) {        
        this.name = name;
        this.address = address;
        this.sports = sports;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public AddressE getAddress() {
        return address;
    }

    public void setAddress(AddressE address) {
        this.address = address;
    }

    public List<SportsE> getSports() {
        return sports;
    }

    public void setSports(List<SportsE> sports) {
        this.sports = sports;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SportFacilityE)) {
            return false;
        }
        SportFacilityE other = (SportFacilityE) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "database.entities.sportFacilityE[ id=" + id + " ]";
    }
    
}
