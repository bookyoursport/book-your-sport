/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package database.service;

import database.entities.AddressE;
import database.entities.SportFacilityE;
import database.entities.SportsE;
import java.util.List;

/**
 * 
 * @author mato
 */
public interface SportsFacilitiesService {
    
    /**
     * Metoda vrati sportovisko podla jeho ID.
     * @param id 
     * @return 
     */
    public SportFacilityE getSportFacilityById(long id);
    
    /**
     * Vytvorenie sporoveho zariadenia v databaze. Zaroven sa vytvori (ak existuje)
     * zaznam adresy v tabulke Address.
     * @param sportFacilityE 
     */
    public void createSportFacility(SportFacilityE sportFacilityE);
    
    /**
     * Odstranenie sportoviska <b>vratane jeho ADRESY</b> z databazy.
     * @param id 
     * @return true ak bolo sportove zariadenie vymazane
     */
    public boolean deleteSportFacility(long id);
    
    /**
     * Metoda vrati adresu podla ID.
     * @param id id adresy.
     * @return 
     */
    public AddressE getAddressById(long addressId);
        
    /**
     * Metoda vrati vsetky sportove zariadenia ulozene v databaze.
     * @return 
     */
    public List<SportFacilityE> getAllSportsFacilities();
    
    /**
     * Modifikacia vsetkych udajov patriacich k adrese {@code addressId}.
     * @param addressId id adresy.
     * @param city nove mesto.
     * @param streetNumber nove cislo ulice.
     * @param streetName novy nazov ulice.
     * @param zipCode nove postove smerovacie cislo.
     */
    public void alterAddress(long addressId, String city, int streetNumber, String streetName, String zipCode);
    
    /**
     * Zoznam vsetkych sportovisk podla typu sportu s id {@code sportId}.
     * @param sportId
     * @return 
     */
    public List<SportFacilityE> getSportsFacilityBySportId(long sportId);
    
    /**
     * Modifikacia nazvu sportoveho zariadenia.
     * @param id
     * @param newName 
     */
    public void alterSportFacilityName(long id, String newName);
    
    /**
     * Modifikacia adresy, na ktorej sa dane sportovisko nachadza.
     * @param sportFacilityId
     * @param newName 
     */
    public void alterSportFacilityAddress(long sportFacilityId, AddressE newAddress);
    
    /**
     * Modifikacia sportov, ktore sa konaju na danom sportovisku.
     * @param id
     * @param newName 
     */
    public void alterSportFacilitySports(long id, List<SportsE> newSports);
    
    /**
     * Modifikacia udajov objektu typu SportFacilityE s id {@code sportFacilityId}
     * podla udajov z objektu {@code newData}
     * @param sportFacilityId
     * @param newData 
     */
    public void alterSportFacility(long sportFacilityId, SportFacilityE newData);
}
