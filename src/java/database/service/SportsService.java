/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package database.service;

import database.entities.SportsE;
import java.util.List;

/**
 *
 * @author mato
 */
public interface SportsService {
    
    /**
     * Vytvorenie zaznamu v databaze s danym sportom.
     * @param sport 
     */
    public void createSport(SportsE sport);
    
    /**
     * Odstranie sportu z databazy.
     * @param sportId 
     * @return true ak odstranenie sportu bolo uspesne
     */
    public boolean deleteSport(long sportId);
    
    /**
     * Metoda vrati sport z databaty podla ID.
     * @param sportId
     * @return 
     */
    public SportsE getSportById(long sportId);
    
    /**
     * Metoda vrati vsetky typy sportov z databazy.
     * @return 
     */
    public List<SportsE> getAllSports();
    
    /**
     * Modifikacia nazvu sportu.
     * @param sportId id sportu, nazov ktoreho potrebujeme zmenit.
     * @param newName 
     */
    public void alterSportName(long sportId, String newName);
    
    /**
     * Metoda vrati true ak v databaze existuje sport s danym menm {@code sportName}
     * @param sportName
     * @return 
     */
    public boolean existsSportWithName(String sportName);
}
