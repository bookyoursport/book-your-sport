/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package database.service.impl;

import database.entities.AddressE;
import database.entities.SportFacilityE;
import database.entities.SportsE;
import database.service.SportsFacilitiesService;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.persistence.EntityManager;

/**
 *
 * @author mato
 */
@ManagedBean(name="sportsFacilitiesService")
@SessionScoped
public class SportsFacilitiesBean implements SportsFacilitiesService{
    @ManagedProperty(value="#{jpaResourceBean}")
    private JPAResourceBean jpaResourceBean;
    
    @Override
    public SportFacilityE getSportFacilityById(long id) {
        EntityManager em = jpaResourceBean.getEMF().createEntityManager();
        try{
            return em.find(SportFacilityE.class, id);
        }
        finally{
            em.close();
        }
    }

    @Override
    public void createSportFacility(SportFacilityE sportFacilityE) {
        EntityManager em = jpaResourceBean.getEMF().createEntityManager();
        try{
            em.getTransaction().begin();            
            em.merge(sportFacilityE); 
            em.getTransaction().commit();
        }finally{
            em.close();
        }
    }

    @Override
    public boolean deleteSportFacility(long id) {
        EntityManager em = jpaResourceBean.getEMF().createEntityManager();
        try{
            em.getTransaction().begin();
            SportFacilityE sportFacility = em.find(SportFacilityE.class, id);        
            em.remove(sportFacility);
            em.getTransaction().commit();
        }
        catch (Exception ex){
            return false;
        }
        finally{
            em.close();
        }
        return true;
    }

    @Override
    public AddressE getAddressById(long addressId) {
        EntityManager em = jpaResourceBean.getEMF().createEntityManager();
        try{
            return em.find(AddressE.class, addressId);
        }
        finally{
            em.close();
        }
    }

    @Override
    public List<SportFacilityE> getAllSportsFacilities() {
        EntityManager em = jpaResourceBean.getEMF().createEntityManager();        
        return em.createQuery("SELECT s FROM SportFacilityE s").getResultList();
    }

    @Override
    public void alterAddress(long addressId, String city, int streetNumber, String streetName, String zipCode) {
        EntityManager em = jpaResourceBean.getEMF().createEntityManager();
        try{
            em.getTransaction().begin();            
            AddressE address = em.find(AddressE.class, addressId);
            updateAddressData(address, new AddressE(city, streetNumber, streetName, zipCode));            
            em.getTransaction().commit();
        }finally{
            em.close();
        }
    }
    
    /**
     * Zmenit adresu sportoviska podla hodnot z objektu {@code newAddress} . 
     * Objekt {@code newAddress} nie je manazovany JPA tj. nie je v databaze v 
     * tabulke Address.
     * 
     * @param sportFacilityId
     * @param newAddress 
     */
    @Override
    public void alterSportFacilityAddress(long sportFacilityId, AddressE newAddress) {
        EntityManager em = jpaResourceBean.getEMF().createEntityManager();
        try{
            em.getTransaction().begin();            
            SportFacilityE sportFacility = em.find(SportFacilityE.class, sportFacilityId);  
            updateAddressData(sportFacility.getAddress(), newAddress);            
            em.getTransaction().commit();
        }finally{
            em.close();
        }
    }
    
    private void updateAddressData(AddressE currentAddress, AddressE newAddress){
        currentAddress.setCity(newAddress.getCity());
        currentAddress.setStreetName(newAddress.getStreetName());
        currentAddress.setStreetNumber(newAddress.getStreetNumber());
        currentAddress.setZipCode(newAddress.getZipCode());
    }

    @Override
    public List<SportFacilityE> getSportsFacilityBySportId(long sportId) {
        EntityManager em = jpaResourceBean.getEMF().createEntityManager();
        
        return em.createQuery(
            "SELECT s FROM SportFacilityE s JOIN s.sports x WHERE x.id = :sportID")
            .setParameter("sportID", sportId)
            .getResultList();
    }
    
    @Override
    public void alterSportFacilityName(long id, String newName) {
        EntityManager em = jpaResourceBean.getEMF().createEntityManager();
        try{
            em.getTransaction().begin();
            SportFacilityE sportFacility = em.find(SportFacilityE.class, id);
            sportFacility.setName(newName);
            em.getTransaction().commit();
        }finally{
            em.close();
        }
    }

    @Override
    public void alterSportFacilitySports(long id, List<SportsE> newSports) {
        EntityManager em = jpaResourceBean.getEMF().createEntityManager();
        try{
            em.getTransaction().begin();
            SportFacilityE sportFacility = em.find(SportFacilityE.class, id);
            sportFacility.setSports(newSports);
            em.getTransaction().commit();
        }finally{
            em.close();
        }
    }
        
    @Override
    public void alterSportFacility(long sportFacilityId, SportFacilityE newData) {
        EntityManager em = jpaResourceBean.getEMF().createEntityManager();
        try{
            em.getTransaction().begin();
            
            SportFacilityE sportFacility = em.find(SportFacilityE.class, sportFacilityId);
            AddressE address = em.find(AddressE.class, sportFacility.getAddress().getId());
            updateAddressData(address, newData.getAddress());             
            sportFacility.setName(newData.getName());
            
            //TODO otestovat
            //SportFacilityE sportFacility = em.find(SportFacilityE.class, sportFacility.ge);
            sportFacility.setSports(newData.getSports());
                    
            em.getTransaction().commit();
        }finally{
            em.close();
        }
    }

    /**
     * @return the jpaResourceBean
     */
    public JPAResourceBean getJpaResourceBean() {
        return jpaResourceBean;
    }

    /**
     * @param jpaResourceBean the jpaResourceBean to set
     */
    public void setJpaResourceBean(JPAResourceBean jpaResourceBean) {
        this.jpaResourceBean = jpaResourceBean;
    }
    
}
