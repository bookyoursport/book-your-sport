/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package database.service.impl;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Tato trieda zaruci jednu instanciu (Application Scoped) EntityManagerFactory 
 * pre celu aplikaciu.
 * 
 * @author mato
 */
@ManagedBean(name="jpaResourceBean")
@ApplicationScoped
public class JPAResourceBean {
    /** Persistance unit name - persistence.xml */
    public final static String PERSISTANCE_UNIT_NAME = "BookYourSportPU";
    /** Referencia na EntityManagerFactory  */
    protected EntityManagerFactory emf;
    
     /**
     * Ziska referenciu na poziadavku.
     * @return
     */
    public EntityManagerFactory getEMF (){
        if (emf == null){
            emf = Persistence.createEntityManagerFactory(PERSISTANCE_UNIT_NAME);
        }
        return emf;
    }
}
