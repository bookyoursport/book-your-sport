/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBeans;

import database.entities.UserE;
import database.service.UserService;
import java.util.ResourceBundle;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Lukas
 */
@ManagedBean(name="loginBean")
@SessionScoped
public class LoginBean {

    private UserE userE;
    private String password;
    private String loginEmail;
    private String forgetEmail;
    private boolean loggedIn = false;
    private boolean adminLoggin = false;
    @ManagedProperty(value = "#{userService}")
    private UserService userService;
    @ManagedProperty(value="#{language}")
    private LanguageBean languageBean;
    @ManagedProperty(value="#{passwdForger}")
    private PasswdForgetBean passwdForget;
 
    public String isValidLogin() {
        userE = userService.getUserByEmail(loginEmail);

        if (userE != null && userE.getPasswd().equals(password) && userE.isAdmin() == false) {
            setLoggedIn(true);
            return "home?faces-redirect=true";
        } else if (userE != null && userE.getPasswd().equals(password) && userE.isAdmin() == true) {
            setLoggedIn(true);
            setAdminLoggin(true);
            return "home?faces-redirect=true";
        } else {
            FacesContext cont = FacesContext.getCurrentInstance();      
            ResourceBundle resBundle = ResourceBundle.getBundle
                    ("localization/web/login", languageBean.getLocale());  
            FacesMessage msg = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR, resBundle.getString("badcredentials"), ""); 
            cont.addMessage(null, msg);  
            
            loggedIn = false;
            return null;
        }

    }
    
    public void forgetPassword() {
        userE = userService.getUserByEmail(getForgetEmail());
        if (userE != null) {
            getPasswdForget().setRecipient(getForgetEmail());
            getPasswdForget().setPassword(userE.getPasswd());
            getPasswdForget().send();
        } else {
            FacesContext cont = FacesContext.getCurrentInstance();      
            ResourceBundle resBundle = ResourceBundle.getBundle
                    ("localization/web/login", languageBean.getLocale());  
            FacesMessage msg = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR, resBundle.getString("bademail"), ""); 
            cont.addMessage(null, msg);
        }
    }

    public String logOut() {
        setLoggedIn(false);       
        setPassword(null);
        setLoginEmail(null);
        setAdminLoggin(false);
        this.userE = null; 
        //pretty:home preto lebo samostatny home nefunguje + niektore stranky 
        //sa nachadzaju o uroven vyssie ako home, napr. /user/details
        return "pretty:home";
    }

    /**
     * @return the userService
     */
    public UserService getUserService() {
        return userService;
    }

    /**
     * @param userService the userService to set
     */
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public UserE getUser() {
        return userE;
    }

    public String getPassword() {
        return password;
    }

    public String getLoginEmail() {
        return loginEmail;
    }

    public void setLoginEmail(String loginEmail) {
        this.loginEmail = loginEmail;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the loggedIn
     */
    public boolean isLoggedIn() {
        if (this.userE == null){
            return false;
        }
        return loggedIn;
    }

    /**
     * @param loggedIn the loggedIn to set
     */
    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    /**
     * @return the adminLoggin
     */
    public boolean isAdminLoggin() {
        if (this.userE == null){
            return false;
        }
        return adminLoggin;
    }

    /**
     * @param adminLoggin the adminLoggin to set
     */
    public void setAdminLoggin(boolean adminLoggin) {
        this.adminLoggin = adminLoggin;
    }

    /**
     * @return the languageBean
     */
    public LanguageBean getLanguageBean() {
        return languageBean;
    }

    /**
     * @param languageBean the languageBean to set
     */
    public void setLanguageBean(LanguageBean languageBean) {
        this.languageBean = languageBean;
    }

    /**
     * @return the forgetEmail
     */
    public String getForgetEmail() {
        return forgetEmail;
    }

    /**
     * @param forgetEmail the forgetEmail to set
     */
    public void setForgetEmail(String forgetEmail) {
        this.forgetEmail = forgetEmail;
    }

    /**
     * @return the passwdForget
     */
    public PasswdForgetBean getPasswdForget() {
        return passwdForget;
    }

    /**
     * @param passwdForget the passwdForget to set
     */
    public void setPasswdForget(PasswdForgetBean passwdForget) {
        this.passwdForget = passwdForget;
    }
}
