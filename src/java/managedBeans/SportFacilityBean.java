/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBeans;

import database.entities.SportFacilityE;
import database.service.SportsFacilitiesService;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import utils.Sorting;

/**
 *
 * @author Gabo
 */
@ManagedBean(name="sportFacilityBean")
@SessionScoped
public class SportFacilityBean {
    private List<SportFacilityE> sportFacilities;
    private SportFacilityE selectedSportFacility;
    @ManagedProperty(value="#{sportsFacilitiesService}")
    private SportsFacilitiesService sportFacilitiyService;
        
    //Getters and Setters-------------------------------------------------------
    public List<SportFacilityE> getSportFacilities() {
        sportFacilities = getSportFacilitiyService().getAllSportsFacilities();
        sportFacilities = Sorting.sortSportsFacilities(sportFacilities);
        return sportFacilities;
    }

    public void setSportFacilities(List<SportFacilityE> sportFacilities) {
        this.sportFacilities = sportFacilities;
    }

    public SportFacilityE getSelectedSportFacility() {
        if(selectedSportFacility == null) {
            selectedSportFacility = getSportFacilities().get(0);
        }
        return selectedSportFacility;
    }

    public void setSelectedSportFacility(SportFacilityE selectedSportFacility) {
        this.selectedSportFacility = selectedSportFacility;
    }

    public SportsFacilitiesService getSportFacilitiyService() {
        return sportFacilitiyService;
    }

    public void setSportFacilitiyService(SportsFacilitiesService sportFacilitiyService) {
        this.sportFacilitiyService = sportFacilitiyService;
    }
}
