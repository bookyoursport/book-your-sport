/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBeans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Gabo
 */
@ManagedBean
@SessionScoped
public class Navigation {
     
    private enum Pages  {
    login, registration, home, contactus, help, table, sports
    }

    public Object getLogin(){
        return Pages.login;
    }
    
    public Object getRegistration(){
        return Pages.registration;
    }
    
    public Object getHome(){
        return Pages.home;
    }
    
    public Object getContactus(){
        return Pages.contactus;
    }
    
    public Object getHelp(){
        return Pages.help;
    }
    
    public Object getTable(){
        return Pages.table;
    }
    
    public Object getSports(){
        return Pages.sports;
    }
}
