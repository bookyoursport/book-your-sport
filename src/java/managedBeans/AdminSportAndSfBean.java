/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBeans;

import database.entities.AddressE;
import database.entities.ReservationE;
import database.entities.SportFacilityE;
import database.entities.SportsE;
import database.entities.UserE;
import database.service.ReservationService;
import database.service.SportsFacilitiesService;
import database.service.SportsService;
import database.service.UserService;
import java.io.Serializable;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import utils.Sorting;

/**
 *
 * @author mato
 */
@ManagedBean
@SessionScoped
public class AdminSportAndSfBean implements Serializable{
    private List<SportsE> allSports;
    private List<SportFacilityE> allSportFacilities;
    
    private SportsE currentSport;
    private String newSportName;  
    
    private SportFacilityE currentSportFacility;
    private SportFacilityE newSportFacility;
        
    @ManagedProperty(value="#{sportsService}")
    private SportsService sportsService;
    @ManagedProperty(value="#{sportsFacilitiesService}")
    private SportsFacilitiesService sportsFacilitiesService;
    @ManagedProperty(value="#{reservationService}")
    private ReservationService reservationService;
    @ManagedProperty(value="#{userService}")
    private UserService userService;
    @ManagedProperty(value="#{language}")
    private LanguageBean languageBean;
    
    private ResourceBundle resBundle;
    private final static String LANG_FILE = "localization/web/administration";

    public AdminSportAndSfBean() {
        this.newSportFacility = new SportFacilityE();
        this.newSportFacility.setAddress(new AddressE());
        this.newSportFacility.setSports(new ArrayList<SportsE>());
    }
            
    public void saveSportFacility(){
        sportsFacilitiesService.createSportFacility(newSportFacility);
        
        String rawMessage = getResBundle().getString("sportfacility.savesuccess"); 
        String messageTxt = MessageFormat.format(rawMessage, newSportFacility.getName());
        
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, messageTxt, "");  
        FacesContext.getCurrentInstance().addMessage(null, msg); 
    }
    
    public void deleteSportFacility(){
        boolean result = sportsFacilitiesService.deleteSportFacility(currentSportFacility.getId());
        if (result){
            String rawMessage = getResBundle().getString("sportfacility.deletesuccess"); 
            String messageTxt = MessageFormat.format(rawMessage, currentSportFacility.getName());
            
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, messageTxt, "");  
            FacesContext.getCurrentInstance().addMessage(null, msg);    
        }
        else{
            List<ReservationE> reservations =  getReservationService().
                    getReservationsBySportFacilityId(currentSportFacility.getId());
            StringBuilder sb = new StringBuilder("");
            for (ReservationE res : reservations){               
                sb.append(res.getTitle());                
                sb.append(", ");
            }
            sb.delete(sb.length()-2, sb.length());
            
            String rawMessage = getResBundle().getString("sportfacility.deleteerror");  
            String messageTxt = MessageFormat.format(rawMessage, currentSportFacility.getName(), sb);
            
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, messageTxt, "");  
            FacesContext.getCurrentInstance().addMessage(null, msg);    
        }
    }
    
    public void saveChangesToSportFacility(){
        sportsFacilitiesService.alterSportFacility(currentSportFacility.getId(), currentSportFacility);
    }
    
    public void saveChangesToSport(){
        sportsService.alterSportName(currentSport.getId(), currentSport.getName());
    }
    
    public void deleteSport(){        
        boolean result = sportsService.deleteSport(getCurrentSport().getId());      
        
        if (result){
            String rawMessage = getResBundle().getString("sport.deletesuccess"); 
            String messageTxt = MessageFormat.format(rawMessage, getCurrentSport().getName());
            
            FacesMessage msg = new FacesMessage(
                    FacesMessage.SEVERITY_INFO, messageTxt, "");  
            FacesContext.getCurrentInstance().addMessage(null, msg);    
        }
        else{
            
            List<SportFacilityE> sf =  sportsFacilitiesService.getSportsFacilityBySportId(getCurrentSport().getId());            
            StringBuilder sbSportFacilies = new StringBuilder("(");
            for (int i=0; i<sf.size(); i++){
                sbSportFacilies.append(sf.get(i).getName());
                if (sf.size()-i!=1){
                    sbSportFacilies.append(", ");
                }
            }
            sbSportFacilies.append(")");           

            List<UserE> users = userService.getUsersBySportId(getCurrentSport().getId());
            StringBuilder sbUsers = new StringBuilder("(");
            for (int i=0; i<users.size(); i++){
                sbUsers.append(users.get(i).getEmail());
                if (users.size()-i!=1){
                    sbUsers.append(", ");
                }
            }
            sbUsers.append(")");
                        
            String rawMessage = getResBundle().getString("sport.deleteerror");  
            String messageTxt = MessageFormat.format(rawMessage, getCurrentSport().getName(), sbSportFacilies, sbUsers);
            
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, messageTxt , "");  
            FacesContext.getCurrentInstance().addMessage(null, msg);    
        }
    }
    
    public void saveSport(){    
        sportsService.createSport(new SportsE(getNewSportName()));
        
        String rawMessage = getResBundle().getString("sport.savesuccess"); 
        String messageTxt = MessageFormat.format(rawMessage, getNewSportName());
        
        FacesMessage msg = new FacesMessage(
                    FacesMessage.SEVERITY_INFO, messageTxt, "");  
        FacesContext.getCurrentInstance().addMessage(null, msg); 
    }
    
    public void validateSportName(FacesContext context, UIComponent component,
        Object value) throws ValidatorException {
        
        int length = value.toString().length();
        //minimalna dlzka nazvu sportu nech je 2
        if (length <= 2){
            throw new ValidatorException(new FacesMessage(
                    FacesMessage.SEVERITY_ERROR, getResBundle().getString("sportvalidator.minimum"),""));            
        }
        //maximalna dlzka nazvu sportu nech je 50
        else if (length >= 50){            
            throw new ValidatorException(new FacesMessage(
                    FacesMessage.SEVERITY_ERROR, getResBundle().getString("sportvalidator.maximum"),""));  
        }
        
        //existuje sport s novym nazvom v databaze ?
        boolean existName = sportsService.existsSportWithName(value.toString());        
        if (existName){
            throw new ValidatorException(new FacesMessage(
                    FacesMessage.SEVERITY_ERROR,getResBundle().getString("sportvalidator.samename"),""));
        }        
    }
    
    /**
     * @return the sports
     */
    public List<SportsE> getAllSports() {
        allSports = Sorting.sortSports(this.sportsService.getAllSports());
        return allSports;
    }
    
    public boolean isEmptyAllSports(){
        if (this.allSports != null){
            if(this.allSports.size() > 0 ){
                return true;
            }            
        }
        return false;
    }
            
    /**
     * @param sports the sports to set
     */
    public void setAllSports(List<SportsE> sports) {
        this.allSports = sports;
    }

    /**
     * @return the currentSport
     */
    public SportsE getCurrentSport() {
        return currentSport;
    }

    /**
     * @param currentSport the currentSport to set
     */
    public void setCurrentSport(SportsE currentSport) {
        this.currentSport = currentSport;
    }

    /**
     * @return the sportsService
     */
    public SportsService getSportsService() {
        return sportsService;
    }

    /**
     * @param sportsService the sportsService to set
     */
    public void setSportsService(SportsService sportsService) {
        this.sportsService = sportsService;
    }

    /**
     * @return the newSportName
     */
    public String getNewSportName() {
        return newSportName;
    }

    /**
     * @param newSportName the newSportName to set
     */
    public void setNewSportName(String newSportName) {
        this.newSportName = newSportName;
    }


    /**
     * @return the sportsFacilitiesService
     */
    public SportsFacilitiesService getSportsFacilitiesService() {
        return sportsFacilitiesService;
    }

    /**
     * @param sportsFacilitiesService the sportsFacilitiesService to set
     */
    public void setSportsFacilitiesService(SportsFacilitiesService sportsFacilitiesService) {
        this.sportsFacilitiesService = sportsFacilitiesService;
    }

    /**
     * @return the sportFacilities
     */
    public List<SportFacilityE> getAllSportFacilities() {
        this.allSportFacilities = sportsFacilitiesService.getAllSportsFacilities();
        this.allSportFacilities = Sorting.sortSportsFacilities(this.allSportFacilities);
        return allSportFacilities;
    }

    /**
     * @param sportFacilities the sportFacilities to set
     */
    public void setAllSportFacilities(List<SportFacilityE> sportFacilities) {
        this.allSportFacilities = sportFacilities;
    }

    /**
     * @return the currentSportFacilities
     */
    public SportFacilityE getCurrentSportFacility() {
        return currentSportFacility;
    }

    /**
     * @param currentSportFacilities the currentSportFacilities to set
     */
    public void setCurrentSportFacility(SportFacilityE currentSportFacilities) {
        this.currentSportFacility = currentSportFacilities;
    }

    /**
     * @return the newSportFacilityE
     */
    public SportFacilityE getNewSportFacility() {
        return newSportFacility;
    }

    /**
     * @param newSportFacilityE the newSportFacilityE to set
     */
    public void setNewSportFacility(SportFacilityE newSportFacilityE) {
        this.newSportFacility = newSportFacilityE;
    }

    /**
     * @return the reservationService
     */
    public ReservationService getReservationService() {
        return reservationService;
    }

    /**
     * @param reservationService the reservationService to set
     */
    public void setReservationService(ReservationService reservationService) {
        this.reservationService = reservationService;
    }

    /**
     * @return the languageBean
     */
    public LanguageBean getLanguageBean() {
        return languageBean;
    }

    /**
     * @param languageBean the languageBean to set
     */
    public void setLanguageBean(LanguageBean languageBean) {
        this.languageBean = languageBean;
        this.resBundle  = ResourceBundle.getBundle(LANG_FILE, getLanguageBean().getLocale());
    }

    /**
     * @return the userService
     */
    public UserService getUserService() {
        return userService;
    }

    /**
     * @param userService the userService to set
     */
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    /**
     * @return the resBundle
     */
    public ResourceBundle getResBundle() {
        return resBundle;
    }
}
