/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBeans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Lukas
 */
@ManagedBean
@SessionScoped
public class AgreementBean {
    
    private boolean agreement;

    /**
     * @return the agreement
     */
    public boolean isAgreement() {
        return agreement;
    }

    /**
     * @param agreement the agreement to set
     */
    public void setAgreement(boolean agreement) {
        this.agreement = agreement;
    }
}
