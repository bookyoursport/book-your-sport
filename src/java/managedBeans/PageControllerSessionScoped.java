/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBeans;

import java.util.Date;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import org.primefaces.event.DateSelectEvent;

/**
 *
 * @author gcervena
 */
@ManagedBean
@SessionScoped
public class PageControllerSessionScoped {
    
    @ManagedProperty(value="#{scheduleController}")
    private ScheduleController scheduleController;
    private Date calendarSelectedDate;
    
        public void calendarHandleDateSelect(DateSelectEvent event){
            System.out.println("----- ----- HANDLING CALENDAR DATE ----- -----");
        calendarSelectedDate = event.getDate();
        scheduleController.navigate();
    }

    public Date getCalendarSelectedDate() {
        if(calendarSelectedDate == null) {
            return new Date();
        }
        return calendarSelectedDate;
    }

    public void setCalendarSelectedDate(Date calendarSelectedDate) {
        this.calendarSelectedDate = calendarSelectedDate;
    }

    public ScheduleController getScheduleController() {
        return scheduleController;
    }

    public void setScheduleController(ScheduleController scheduleController) {
        this.scheduleController = scheduleController;
    }
    
}
