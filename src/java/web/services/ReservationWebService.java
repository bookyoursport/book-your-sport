/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package web.services;

import database.entities.ReservationE;
import database.service.impl.ReservationBean;
import java.util.Date;
import java.util.List;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author Gabo
 */
@WebService(serviceName = "ReservationWebService")
public class ReservationWebService {

    /**
     * Web service operation
     */
    @WebMethod(operationName = "getReservationsByDate")
    public List<ReservationE> getReservationsByDate(@WebParam(name = "date") Date date) {
        ReservationBean reservation = new ReservationBean();
        List<ReservationE> reservationList = reservation.getReservationsByResDate(date);
        return reservationList;
    }
}
