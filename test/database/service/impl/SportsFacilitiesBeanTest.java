/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package database.service.impl;

import database.entities.AddressE;
import database.entities.SportFacilityE;
import database.entities.SportsE;
import database.service.SportsService;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author mato
 */

/////////////
/////////////
/////////////  instance.setJpaResourceBean(jPAResourceBean); v normalnej aplikacii netreba davat, lebo uz by mala byt vytvorena instancia v celej aplikacii
/////////////  
/////////////

public class SportsFacilitiesBeanTest {
    
    private JPAResourceBean jPAResourceBean;
    
    public SportsFacilitiesBeanTest() {
        jPAResourceBean = new JPAResourceBean();
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetSportFacilityById() {
        //OK funguje
        System.out.println("getSportFacilityById");
        long id = 2L;
        
        SportsFacilitiesBean instance = new SportsFacilitiesBean();
        instance.setJpaResourceBean(jPAResourceBean);
                
        SportFacilityE result = instance.getSportFacilityById(id);
        System.out.println("Sportove zariadenie: " + result.getName());
    }

    @Test
    public void testCreateSportFacility() {
        //OK funguje
        System.out.println("createSportFacility");
                
        SportFacilityE sportFacilityE = new SportFacilityE(
                "Super zariadenie 2", new AddressE("Snina", 10, "SNP", "0601"), null);

        SportsFacilitiesBean instance = new SportsFacilitiesBean();
        instance.setJpaResourceBean(jPAResourceBean);

//        AddressE address = instance.getAddressById(2);
//        SportFacilityE sportFacilityE = new SportFacilityE("Super zariadenie", address, null);

        instance.createSportFacility(sportFacilityE);

        SportsBean sb = new SportsBean();
        sb.setJpaResourceBean(jPAResourceBean);

        SportsE sportsE0 = sb.getSportById(1);
        SportsE sportsE1 = sb.getSportById(2);
        //SportsE sportsE4 = sb.getSportById(4);

        List<SportsE> listSportsE = new ArrayList<SportsE>();
        listSportsE.add(sportsE0);
        listSportsE.add(sportsE1);
       // listSportsE.add(sportsE4);
        System.out.println("SPORTY: " + listSportsE);

        SportFacilityE sportFacilityE1 = new SportFacilityE(
                "Mega cool zariadenie 3", new AddressE("Bela", 10, "SNP", "0456"), listSportsE);

        instance.createSportFacility(sportFacilityE1);
    }

    @Test
    public void testDeleteSportFacility() {
        //OK funguje
        System.out.println("deleteSportFacility");
        long id = 602;
        
        SportsFacilitiesBean instance = new SportsFacilitiesBean();
        instance.setJpaResourceBean(jPAResourceBean);
        
        instance.deleteSportFacility(id);
    }

    @Test
    public void testGetAddressById() {
        //OK funguje
        System.out.println("getAddressById");
        long addressId = 2L;
        
        SportsFacilitiesBean instance = new SportsFacilitiesBean();
        instance.setJpaResourceBean(jPAResourceBean);
        
        AddressE result = instance.getAddressById(addressId);
        System.out.println(result.getCity() + ", " + result.getStreetName());
    }

    @Test
    public void testGetAllSportsFacilities() {
        //OK funguje
        System.out.println("getAllSportsFacilities");
        
        SportsFacilitiesBean instance = new SportsFacilitiesBean();
        instance.setJpaResourceBean(jPAResourceBean);
        
        List result = instance.getAllSportsFacilities();
        System.out.println(result);
    }

    @Test
    public void testAlterAddress() {
        //OK funguje
        System.out.println("alterAddress");
        
        long addressId = 1;
        String city = "Podskalka";
        int streetNumber = 111;
        String streetName = "Ciganska";
        String zipCode = "12345";
        
        SportsFacilitiesBean instance = new SportsFacilitiesBean();
        instance.setJpaResourceBean(jPAResourceBean);
        
        instance.alterAddress(addressId, city, streetNumber, streetName, zipCode);
    }

    @Test
    public void testGetSportsFacilityBySportId() {
        //OK funguje
        System.out.println("getSportsFacilityBySportId");
        long sportId = 5L;
        
        SportsFacilitiesBean instance = new SportsFacilitiesBean();
        instance.setJpaResourceBean(jPAResourceBean);
        
        List result = instance.getSportsFacilityBySportId(sportId);
        System.out.println(result);
    }

    @Test
    public void testAlterSportFacilityName() {
        //OK funguje
        System.out.println("alterSportFacilityName");
        long id = 1L;
        String newName = "Old Trafford";
        
        SportsFacilitiesBean instance = new SportsFacilitiesBean();
        instance.setJpaResourceBean(jPAResourceBean);
        
        instance.alterSportFacilityName(id, newName);
    }

    @Test
    public void testAlterSportFacilityAddress() {
        //OK funguje        
        System.out.println("alterSportFacilityAddress");
        long id = 501;
        
        SportsFacilitiesBean instance = new SportsFacilitiesBean();
        instance.setJpaResourceBean(jPAResourceBean);
        
        AddressE address = new AddressE("Prievidza", 45, "Pod kosom" , "05313");    
        
        instance.alterSportFacilityAddress(id, address);
    }

    @Test
    public void testAlterSportFacilitySports() {
        //OK funguje
        System.out.println("alterSportFacilitySports");
        long id = 2;
        
        SportsFacilitiesBean instance = new SportsFacilitiesBean();
        instance.setJpaResourceBean(jPAResourceBean);
        
        SportFacilityE sf = instance.getSportFacilityById(id);
        List<SportsE> s = sf.getSports();
        
        SportsBean sb = new SportsBean();
        sb.setJpaResourceBean(jPAResourceBean);
        SportsE ns = sb.getSportById(3);
        
//        for(SportsE sport : s){
//            System.out.println("sporty:" + sport.getName());
//        }
        
        s.add(ns);
        
        instance.alterSportFacilitySports(id, s);
    }

}
